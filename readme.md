1. Project Summary:

	The project implements a rock paper scissors game among two PCB programmed like players and other third PCB programmed
	like the referee, the code is designed for can to be programmed like a referee as well as player, this is controlled 
        with the definition of some of the next macros (REFEREE_PROGRAM y PLAYER_PROGRAM), the PCB play a role according to 
        the macro that is defined in the program when this is charged.Both roles were programmed describing the process as a 
        state machine.


2. HW description>
	
	2.1 Links to MCUs datasheet
	
		- PIC32MX470
		  http://www.microchip.com.tw/Data_CD/Datasheet/32-Bits/61185B.pdf

		-PIC32MX470 Curiosity
		  https://www.mouser.com/datasheet/2/268/70005283A-1075423.pdf


3. Serial commands: 

	3.1 Link commands source/header file

	3.2 Serial command file. Program.
		YAT
		https://sourceforge.net/projects/y-a-terminal/

	3.3 Serial Configuration UART1:
		Baudrate: 115200
		Data Bits: 8
		Parity:	None
		Stop Bits: 1
 
		
4. Prerequisites:
	
	4.1 SDK version: 3.61

	4.2 IDE: MPLAB X IDE

	4.3 Compiler: XC32 v1.42

	4.4 Project Configuration:
		
		Device: PIC32MX470F512H.
		Hardware Tool: Starter Kit PIC32MX470 Family. 

5. Versioning: 
	
	5.1 V1.0.20191226

	5.2 State Machines used to do a RPS between two players and a referee using via UART comunication.

6. Authors: 

	6.1 Project staff
		Author: Oscar Mario Loaiza Echeverri.
		
	6.2 Maintainer contact:
		email: oscar.loaiza@titoma.com


